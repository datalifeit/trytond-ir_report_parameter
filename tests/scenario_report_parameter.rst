===============================
Party report_parameter scenario
===============================

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import config, Model, Wizard

Install intrastat::

    >>> config = activate_modules('ir_report_parameter')

Create party::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party')
    >>> party.save()

Create parameter set::
    
    >>> Parameter_set = Model.get('ir.action.report.parameter.set')
    >>> parameter_set = Parameter_set(name='Parameter set')
    >>> parameter_set.save()

Create record::

    >>> Record = Model.get('ir.action.report')
    >>> report = Record(name='Report', report_name='Report Name', parameter_set=parameter_set)
    >>> report.save()

Create party report::

    >>> Party_report = Model.get('party.report_parameter')
    >>> party_report = Party_report(party=party, report=report)
    >>> party_report.save()
    >>> len(Party_report.find([]))
    1
    >>> party_report2 = Party_report(party=party,report=report)
    >>> party_report2.save()
    Traceback (most recent call last):
    ...
    trytond.model.modelsql.SQLConstraintError: Party must be unique per Report. - 